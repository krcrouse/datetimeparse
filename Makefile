build:
	python3 setup.py build_ext -if

# -- all: build 

.PHONY: sphinx docs

sphinx docs: /dev/null
	echo "Making docs"
	sphinx-build docs/ sphinx

git:
	echo "Remember that make doesn't commit any uncommited changes, for your safety"
	git push

test:
	py.test tests

dist: test
	# build
	#python3 setup.py sdist bdist_wheel
	python3 setup.py sdist bdist_wheel
	# this build is not general enough for pypi, so standardize it into manylinux2014 
	# interactive terminal: docker run -i -t -v `pwd`:/io quay.io/pypa/manylinux2014_x86_64 /bin/bash
	ls -1 dist/*whl | xargs -Ifoo docker run -v `pwd`:/io quay.io/pypa/manylinux2014_x86_64 auditwheel repair /io/foo -w /io/wheelhouse
	rm dist/*linux_x86_64.whl
	mv -f wheelhouse/* dist

deploy: 
	echo "Release version to pypi: " `cat VERSION.txt`
	twine upload dist/*
	python3 util/update_version.py -n
	echo "Next version: " `cat VERSION.txt`

clean:
	rm -rf dist/* 
	rm -r build 
	rm -f cythoonlib/datetimeparse.c datetimeparse.*.so
