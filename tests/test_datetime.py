import sys
import datetime
import pytest
import isodate

sys.path.insert(0,'.')
import datetimeparse

now = datetime.datetime.now()
utcnow = datetime.datetime.now(datetime.timezone.utc)
herenow = datetime.datetime.now(datetime.timezone.utc)

@pytest.mark.parametrize(
    "string",
    [
        '1985-04-12T23:20:50.52Z',
        '1996-12-19T16:39:57-08:00',
        '1990-12-31T23:59:60Z',
        '1990-12-31T15:59:60-08:00',
        '1937-01-01T12:00:27.87+00:20',                
    ])
def test_rfc3339_examples(string):
    assert datetimeparse.from_string(string)
    assert datetimeparse.is_rfc3339ish(string)
    assert datetimeparse.is_rfc3339(string)


@pytest.mark.parametrize(
    "string",
    [
        '0001/12/25 14-38-57',
        '2001-02-25_14.38.57',
        '2000.02.01*14:38:57',
        '2000_02_01_00_00_57',        
        '2000_02_01X00_00_57',        
        '2000 02 01 00 00 57',        
    ])
def test_strictness(string):    
    variants = [
        string,
        string + 'z',
        string + 'Z',
        string + '+00:00',
        string + '-00:00',
        string + '+0:00',
        string + '-0:00',           
        string + '+12:35',
        string + '-10:15',
        string + '+5:20',
        string + '-3:30',           
        string + '+12:00',
        string + '-10:00',
        string + '+5:00',
        string + '-3:00',           
        string + '+0000',
        string + '-00',
        string + '+000',
        string + '-0',           
        string + '+1235',
        string + '-1015',
        string + '+0520',
        string + '-0330',           
        string + '+520',
        string + '-330',           
        string + '+12',
        string + '-10',
        string + '+05',
        string + '-3',           
        string + '+7',           
    ]
    for variant in variants:                        
        assert datetimeparse.is_rfc3339ish(variant)
        assert datetimeparse.is_rfc3339(variant)
        assert datetimeparse.from_string(variant)
        assert not datetimeparse.is_rfc3339ish(variant, strict=True)
        assert not datetimeparse.is_rfc3339(variant, strict=True)
        with pytest.raises(ValueError):
            datetimeparse.from_string(variant, strict=True)
        
@pytest.mark.parametrize(
    "string",
    [
        '2021-12-25T14:38:57',
        '0001-12-25T14:38:57',
        '2001-02-25T14:38:57',
        '2000-02-01T14:38:57',
        '2000-02-01T00:38:57',
        '2000-02-01T00:00:57',
        '2000-02-01T00:00:00',
        '2000-02-01T00:05:00',
        '2000-02-01T05:10:00',
        '2000-02-01T05:10:05',
        '2000-02-01T23:10:00',
        '2000-02-01T23:59:59',
    ])
def test_accuracy(string):    
    tztest(string)
    for msplace in range(1,7):
        tztest(string + '.' + ('0' *msplace))
        tztest(string + '.' + ('1' *msplace))
        tztest(string + '.1' + ('0' *(msplace-1)))
        tztest(string + '.' + ('0' *(msplace-1) + '1'))
        tztest(string + ',' + ('0' *msplace))
        tztest(string + ',' + ('1' *msplace))
        tztest(string + ',1' + ('0' *(msplace-1)))
        tztest(string + ',' + ('0' *(msplace-1) + '1'))
            
def tztest(string):
    variants = [
        string + '+00:00',
        string + '-00:00',
        string + '+12:35',
        string + '-10:15',
        string + '+12:00',
        string + '-10:00',
        string + '+0000',
        string + '-00',
        string + '+1235',
        string + '-1015',
        string + '+0520',
        string + '-0330',           
        string + '+12',
        string + '-10',
        string + '+05',
    ]

    for variant in variants:
        print(variant)
        reference = isodate.parse_datetime(variant)
        assert datetimeparse.is_rfc3339ish(variant), f"'{variant}' fails is_rfc3339ish'"
        assert datetimeparse.is_rfc3339(variant), f"'{variant}' fails is_rfc3339_format'"
        assert datetimeparse.from_string(variant) == reference, f"{variant} fails - Ref: {reference}; datetimeparse: " + str(datetimeparse.from_string(variant))
        assert datetimeparse.is_rfc3339ish(variant, strict=True), f"'{variant}' fails is_rfc3339ish with strict"
        assert datetimeparse.is_rfc3339(variant, strict=True), f"'{variant}' fails is_rfc3339_format with strict"
        assert datetimeparse.from_string(variant, strict=True) == reference, f"{variant} fails - Ref: {reference}; datetimeparse: " + str(datetimeparse.from_string(variant))


@pytest.mark.parametrize(
    "string",
    [
        '2021-12-25T14:38:57',
        '0001-12-25T14:38:57',
        '2001-02-25T14:38:57',
        '2000-02-01T14:38:57',
        '2000-02-01T00:38:57',
        '2000-02-01T00:00:57',
        '2000-02-01T00:00:00',
        '2000-02-01T00:05:00',
        '2000-02-01T05:10:00',
        '2000-02-01T05:10:05',
        '2000-02-01T23:10:00',
        '2000-02-01T23:59:59',
    ])
def test_zulu(string):
    zulutest(string)
    for msplace in range(1,7):
        zulutest(string + '.' + ('0' *msplace))
        zulutest(string + '.' + ('1' *msplace))
        zulutest(string + '.1' + ('0' *(msplace-1)))
        zulutest(string + '.' + ('0' *(msplace-1) + '1'))
        zulutest(string + ',' + ('0' *msplace))
        zulutest(string + ',' + ('1' *msplace))
        zulutest(string + ',1' + ('0' *(msplace-1)))
        zulutest(string + ',' + ('0' *(msplace-1) + '1'))
                        
def zulutest(string):
    refzulu = isodate.parse_datetime(string + 'Z')
    variants = [
        string + 'Z',
        string + 'z',
        string + '+00:00',
        string + '-00:00',
        string + '+0000',
        string + '-0000',
        string + '-00',
        string + '+00',
    ]
    nonstrict_variants = [
        string,
        string + '+0:00',
        string + '-0:00',
        string + '-0',
        string + '-000',
        string + '+0',
        string + '+000',
    ]

    for variant in variants:
        print(variant)
        assert datetimeparse.is_rfc3339ish(variant), f"'{variant}' fails is_rfc3339ish'"
        assert datetimeparse.is_rfc3339(variant), f"'{variant}' fails is_rfc3339_format'"
        assert datetimeparse.from_string(variant) == refzulu, f"Z fails - Ref: {refzulu}; datetimeparse: " + str(datetimeparse.from_string(variant))
        assert datetimeparse.is_rfc3339ish(variant, strict=True), f"'{variant}' fails is_rfc3339ish with strict"
        assert datetimeparse.is_rfc3339(variant, strict=True), f"'{variant}' fails is_rfc3339_format with strict"

    for variant in nonstrict_variants:
        print(variant)
        assert datetimeparse.is_rfc3339ish(variant), f"'{variant}' fails is_rfc3339ish (nonstrict)"
        assert datetimeparse.is_rfc3339(variant), f"'{variant}' fails is_rfc3339_format (nonstrict)"
        assert datetimeparse.from_string(variant) == refzulu, f"Z fails - Ref: {refzulu}; datetimeparse: " + str(datetimeparse.from_string(variant))
        assert not datetimeparse.is_rfc3339ish(variant, strict=True), f"'{variant}' passes is_rfc3339ish (strict) but should fail"
        assert not datetimeparse.is_rfc3339(variant, strict=True), f"'{variant}' passes is_rfc3339_format (strict) but should fail"

        # -- test strict
        with pytest.raises(ValueError):
            datetimeparse.from_string(variant, strict=True)
